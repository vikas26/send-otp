<?php
ini_set('display_errors', 1);
$to = trim($_POST["mobile_number"]);
$message = trim($_POST["message"]);

$mobile_length = 10;

$json_response = array(
    'status' => 200, 
    'mobile_number' => $to,
    'message' => "SMS successfully sent" 
);

if (strlen($to) != $mobile_length) {
    $json_response["status"] = 501;
    $json_response["message"] = "Invalid mobile number";
    echo json_encode($json_response);
    exit();
}

$to = "91".$to;

$url = 'https://rest.nexmo.com/sms/json?'.
	'api_key=2ab8ed24'.
        '&api_secret=2c1ad1a78db58c00'.
        '&to='.$to.
        '&from='.'NEXMO'.
        '&text='.urlencode($message);

file_get_contents($url);

echo json_encode($json_response);
exit();
?>
