/* show loader */
function ShowLoader (message) {
    $(".loader-overlay .loader-message").text (message);
    $(".loader-overlay").show();
}

/* hide loader */
function HideLoader () {
    $(".loader-overlay").hide();
}

/* custom accordion toggle */
$("#accordion").on('click', '.tab-heading', function() {
    this_el = $(this).attr("data-toggle");
    $(".tab-heading.active").filter(function(){
        if (this_el != $(this).attr("data-toggle")){
            $($(this).attr("data-toggle")).slideUp();
            $(this).removeClass("active");
        }
    });
    $($(this).attr("data-toggle")).slideToggle();
    if ($(this).hasClass("active")) {
        $(this).removeClass("active");
    } else {
        $(this).addClass("active");
    }
});

/* on click of send message button */
$(".tab-collapse").on('click', '.send-message', function () {
    var mobile = $(this).data("mobile");
    $(".send-message-popup input.mobile-number")[0].value = mobile;
    $(".send-message-popup input.otp-message")[0].value = "Hi. Your OTP is: "+(("" + Math.random()).substring(2,8));
    $(".send-message-popup").show();
});

/* hide send otp popup */
$(".send-message-popup").on('click', '.close-button', function () {
    $(".send-message-popup").hide();
});

/* send otp */
$(".send-message-popup").on('click', '.send-otp-button', function () {
    ShowLoader ("Sending...");
    $.ajax({
        method: "POST",
        url: "SendMessage.php",
        data: {
            mobile_number: $(".send-message-popup input.mobile-number")[0].value,
            message: $(".send-message-popup input.otp-message")[0].value
        },
        dataType : "application/json"
    }).always (function(json_response) {
        var response = JSON.parse(json_response.responseText);
        HideLoader ();
        console.log(response);
        if (response["status"] == 200) {
            // success
            $(".send-message-popup").hide();
            alert (response["message"]);
        } else {
            // error
            alert (response["message"]);
        }
    });
});