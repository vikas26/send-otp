/* on click of send message button */
$(".otp-logs-container").on('change', 'select.log-count', function () {
    var url = location.href.split("?")[0];
    var url_param = location.search.replace("?","").split("&");
    var mobile = "";

    // check get params
    for (id in url_param) {
        if (url_param[id].split("=")[0] == "mobile") {
            mobile = "mobile="+url_param[id].split("=")[1];
        }
    }

    // add page number and count to get param
    url += "?page=1&count="+parseInt(this.value);

    // add mobile number if already there
    if (mobile.length > 0) {
        url += "&" + mobile;
    }
    // reload page
    location.href = url;
});