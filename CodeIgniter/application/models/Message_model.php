<?php
class Message_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // send sms
    public function send_message ($data) {
        $data['to'] = "91".$data['to'];
        $url = 'https://rest.nexmo.com/sms/json?'.
                'api_key=2ab8ed24'.
                '&api_secret=2c1ad1a78db58c00'.
                '&to='.$data['to'].
                '&from='.'NEXMO'.
                '&text='.urlencode($data['message']);

        file_get_contents($url);

        // log record
        $this->manage_log ($data);
    }

    // logs sent otp
    function manage_log ($data) {
        $table_data = array (
            'mobile' => $data['to'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'otp' => $data['otp'],
            'created_time' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('sent_otp_log', $table_data);
    }

    // get sent otp logs page wise
    public function get_logs ($page, $count, $mobile) {
        $this->db->select('first_name, last_name, created_time, otp');
        $this->db->from('sent_otp_log');
        if (strlen($mobile) == 12) {
            $this->db->where("mobile", $mobile);
        }
        $this->db->order_by('created_time', 'desc');
        $this->db->limit($count, (($page - 1) * $count));
        $query = $this->db->get(); 
        return $query->result();
    }

    // get sent otp total logs
    public function total_logs ($mobile) {
        $this->db->select('count(*) as count');
        $this->db->from('sent_otp_log');
        if (strlen($mobile) == 12) {
            $this->db->where("mobile", $mobile);
        }
        $query = $this->db->get(); 
        return $query->row()->count;
    }

}