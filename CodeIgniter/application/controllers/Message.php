<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model ('message_model');
    }

    public function index() {
        // input values
        $data = array(
            'to' => trim($this->input->post("mobile_number")),
            'message' => trim($this->input->post("message")),
            'first_name' => trim($this->input->post("first_name")),
            'last_name' => trim($this->input->post("last_name")),
            'otp' => trim($this->input->post("otp")),
        );

        // mobile no. length for validation
        $mobile_length = 10;

        // response
        $json_response = array(
            'status' => 200, 
            'mobile_number' => $data['to'],
            'message' => "SMS successfully sent" 
        );

        // mobile number validation
        if (strlen($data['to']) != $mobile_length) {
            $json_response["status"] = 501;
            $json_response["message"] = "Invalid mobile number";
            echo json_encode($json_response);
            exit();
        }

        // send message
        $this->message_model->send_message ($data);

        // return response
        echo json_encode($json_response);
    }

    // shows otp logs
    public function otp_logs () {
        // get input params
        $page = $this->input->get('page');
        $count = $this->input->get('count');
        $mobile = $this->input->get('mobile') ? "91".$this->input->get('mobile') : "";
        $new_page = $page ? $page : 1;
        $new_count = $count ? $count : LOG_COUNT;

        // collect log data
        $data['logs'] = $this->message_model->get_logs ($new_page, $new_count, $mobile);
        $data['total'] = $this->message_model->total_logs ($mobile);
        $data['page'] = $new_page;
        $data['count'] = $new_count;
        $data['pages'] = ceil($data['total']/$data['count']);
        $data['pages'] = $data['pages'] < 1 ? 1 : $data['pages'];
        $this->load->view('logs', $data);
    }
}
