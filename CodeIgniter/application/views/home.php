<!DOCTYPE html>
<html>
<head>
    <title>Send OTP</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common.css">
</head>
<body>
    <!-- import contact list -->
    <?php 
        $string = file_get_contents("assets/json/PhoneNumbers.json");
        $contacts = json_decode($string, true);
    ?>

    <!-- page heading -->
    <div class="page-heading text-center">
        <h3>Send OTP to contacts</h3>
        <a href="<?php echo base_url();?>index.php/message/otp_logs">View logs</a>
    </div>

    <!-- contact list accordion -->
    <div id="accordion">
        <?php foreach ($contacts["user_data"] as $key => $contact) { ?>

            <!-- contact heading -->
            <div class="tab-heading" data-toggle="<?php echo '#collapse_'.$key; ?>">
                <?php echo $contact['first_name']." ".$contact['last_name']; ?>
            </div>

            <!-- contact details -->
            <div id="<?php echo 'collapse_'.$key; ?>" class="tab-collapse">
                <span class="phone-number"><?php echo $contact['phone_number']; ?></span>
                <span class="user-action-btn-container">
                    <button class="clickable-button send-message" data-firstname="<?php echo $contact['first_name']; ?>" data-lastname="<?php echo $contact['last_name']; ?>" data-mobile="<?php echo $contact['phone_number']; ?>">Send Message</button>
                    <a href="<?php echo base_url();?>index.php/message/otp_logs?mobile=<?php echo $contact['phone_number']; ?>">Logs</a>
                </span>
            </div>

        <?php } ?>
    </div>

    <!-- Send message popup -->
    <div class="send-message-popup popup-window">
        <div class="popup-body">
            <div class="popup-header">
                <span class="heading">Send Message</span>
                <span class="close-button">X</span>
            </div>
            <div class="popup-content">
                <input class="mobile-number" type="number" value="" disabled>
                <input class="otp-message" type="text" value="" disabled>
                <button class="clickable-button send-otp-button">Send</button>
            </div>
        </div>
    </div>

    <?php echo $this->load->view('partials/loader',array(),TRUE); ?>
    <?php echo $this->load->view('partials/scripts',array(),TRUE); ?>
    <script src="<?php echo base_url();?>assets/js/common.js"></script>

</body>
</html>