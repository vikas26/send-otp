<!DOCTYPE html>
<html>
<head>
    <title>OTP Logs</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common.css">
</head>
<body>

    <!-- page heading -->
    <div class="page-heading text-center">
        <h3>Sent OTP's</h3>
        <a href="<?php echo base_url();?>">Send OTP</a>
    </div>

    <!-- contact list accordion -->
    <div class="otp-logs-container">
        <table class="table table-striped table-bordered">
            <thead>
                <th>S.no</th>
                <th>Full name</th>
                <th>Sent OTP</th>
                <th>Time</th>
            </thead>
            <tbody>
                <?php foreach ($logs as $key => $log) { ?>
                    <tr>
                        <td><?php echo $key+1; ?></td>
                        <td><?php echo $log->first_name." ".$log->last_name;; ?></td>
                        <td><?php echo $log->otp; ?></td>
                        <td><?php echo $log->created_time; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <div class="text-center">
            <select class="log-count">
                <option value="2" <?php echo $count==2 ? 'selected' : ''?>>2</option>
                <option value="10" <?php echo $count==10 ? 'selected' : ''?>>10</option>
                <option value="50" <?php echo $count==50 ? 'selected' : ''?>>50</option>
                <option value="100" <?php echo $count==100 ? 'selected' : ''?>>100</option>
            </select>
            <span>Page : <?php echo $page; ?>/<?php echo $pages; ?></span>
        </div>
    </div>

    <?php echo $this->load->view('partials/loader',array(),TRUE); ?>
    <?php echo $this->load->view('partials/scripts',array(),TRUE); ?>
    <script src="<?php echo base_url();?>assets/js/logs.js"></script>

</body>
</html>